<?php get_header(); ?>

<section id="content">
	<div class="container">
		<div class="row">
			<?php 
			
				if (have_posts()) : while (have_posts()) : the_post(); 
					
						$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
			
			?>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 archive-post-featured-image">
				<a href="<?php echo get_permalink(); ?>">
					<img src="<?php echo isset($the_url[0]) ? $the_url[0] : get_template_directory_uri().'/img/placeholder-img.jpg'; ?>" class="img-responsive" width="" height="" alt="Featured image of <?php the_title(); ?>" />
				</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 archive-post-content">
				<ul class="list-unstyled category-list">
				<?php 
					
					$categories = get_the_category($post->ID);
					if(!empty($categories)){
						$e = count($categories); $n=1;
						foreach($categories as $category){
							?>
							<li>
								<a href="<?php echo esc_url(get_category_link( $category->term_id )); ?>"><?php echo $category->name; ?></a><?php echo $n<$e ? '<span class="category-list-separator">|</span>' : '';  ?>
							</li>
							<?php
							$n++;
						}
					}
					
				?>
				</ul>
				<ul class="list-unstyled archive-post-data">
					<li>
						<?php echo get_the_date(); ?>	
					</li>
					<li>|</li>
					<li>
						By <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a>
					</li>
				</ul>
				<div class="archive-post-title">
					<a href="<?php echo get_permalink(); ?>">
						<h3><?php the_title(); ?></h3>
					</a>
				</div>
				<?php the_excerpt(); ?>
				<div class="archive-post-read-more">
					<a href="<?php echo get_permalink(); ?>">
						Read More
					</a>
				</div>
			</div>
			<?php
				endwhile;
				endif;
			?>
		</div>
		<div class="row">
			<div class="pagination">
				<?php echo paginate_links(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>