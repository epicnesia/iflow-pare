<?php 
/**
 * Template Name: About Page
 *
 * Description: A custom page template for displaying about us.
 *
 * @package iFlow
 * @since 1.0
 */ 
?>

<?php get_header(); ?>

<section id="content">
	<?php 
		
		if (have_posts()) : while (have_posts()) : the_post(); 

	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
					<img class="about-img" src="<?php echo get_template_directory_uri().'/img/about/meeting.jpg'; ?>" width="" height="" alt="Metting image" />
					<img class="about-img" src="<?php echo get_template_directory_uri().'/img/about/urban.jpg'; ?>" width="" height="" alt="Urban image" />
					<img class="about-img" src="<?php echo get_template_directory_uri().'/img/about/whiteboard.jpg'; ?>" width="" height="" alt="Whiteboard image" />
					<img class="about-img" src="<?php echo get_template_directory_uri().'/img/about/bicycle.jpg'; ?>" width="" height="" alt="Bicycle image" />
					<img class="about-img" src="<?php echo get_template_directory_uri().'/img/about/teacher.jpg'; ?>" width="" height="" alt="Teacher image" />
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<h1 class="single-post-title"><?php the_title(); ?></h1>
					<div class="single-post-title-divider"></div>
					<?php the_content(); ?>
				</div>
			</div>

			<?php
			endwhile;
			endif;
			?>
			
		</div>
	</div>
</section>

<?php get_footer(); ?>