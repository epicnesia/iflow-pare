<?php
/**
 * The sidebar containing the primary widget area
 *
 * Displays on posts and pages.
 *
 * If no active widgets are in this sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage iFLow
 * @since iFlow 1.0
 */

if ( is_active_sidebar( 'sidebar-1' ) ) :
?>
<div class="row">
	<div id="sidebar" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="row">
			<?php dynamic_sidebar('sidebar-1'); ?>
		</div>
	</div>
</div>
<?php endif; ?>