<?php get_header(); ?>

<section id="content">
	<div class="container">
		<div class="row first-row">
			<div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
				<div class="col-lg-12 tile-item tile-exclusive-program" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/home/exclusive.jpg');">
					<div class="pull-bottom">
						<h2 class="main-title"><a href="<?php echo get_page_link(7); ?>">EXCLUSIVE<br />PROGRAMS</a></h2>
						<a href="<?php echo get_page_link(7); ?>" class="tile-btn">ALL PROGRAMS</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="col-lg-12 tile-item tile-intensive-program" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/home/intensive-program.jpg');">
					<div class="pull-bottom">
						<h2 class="basic-title"><a href="<?php echo get_page_link(7); ?>">INTENSIVE PROGRAM FOR SCHOOL</a></h2>
						<a href="<?php echo get_page_link(7); ?>" class="tile-btn">ALL PROGRAMS</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row second-row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="col-lg-12 tile-item tile-english-for-hijaber" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/home/english-for-hijaber.jpg');">
					<div class="pull-bottom">
						<h2 class="basic-title"><a href="<?php echo get_page_link(7); ?>">ENGLISH FOR HIJABERS</a></h2>
						<a href="<?php echo get_page_link(7); ?>" class="tile-btn">ALL PROGRAMS</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="col-lg-12 tile-item tile-speaking-program" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/home/speaking-program.jpg');">
					<div class="pull-bottom">
						<h2 class="basic-title"><a href="<?php echo get_page_link(7); ?>">SPEAKING PROGRAMS</a></h2>
						<a href="<?php echo get_page_link(7); ?>" class="tile-btn">ALL PROGRAMS</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="col-lg-12 tile-item tile-girl-movement-camp" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/home/girls-movement-camp.jpg');">
					<div class="pull-bottom">
						<h2 class="basic-title"><a href="<?php echo get_page_link(7); ?>">GIRLS MOVEMENT CAMP</a></h2>
						<a href="<?php echo get_page_link(7); ?>" class="tile-btn">ALL PROGRAMS</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>