<?php

/**
 * =========================================================================================
 * Iflow template functions and definitions
 * =========================================================================================
 */

require_once ('inc/wp_bootstrap_navwalker/wp_bootstrap_navwalker.php');
require_once ('inc/iflow_functions.php');
// Bootstrap Nav Walker

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since iFLow 1.0
 */
if (!function_exists('iflow_setup')) :
	function iflow_setup() {

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support('post-thumbnails');
		
		add_image_size('iflow-archive', 587, 500, true); // Featured image in Archive

		// Register navigation menu
		register_nav_menu('primary', __('Primary navigation', 'iflow'));

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption', ));
	}

endif; // iflow_setup
add_action('after_setup_theme', 'iflow_setup');

if (!function_exists('iflow_fonts_url')) :
	/**
	 * Register Google fonts for iFLow.
	 *
	 * Create your own iflow_fonts_url() function to override in a child theme.
	 *
	 * @since iFLow 1.0
	 *
	 * @return string Google fonts URL for the theme.
	 */
	function iflow_fonts_url() {
		$fonts_url = '';
		$fonts = array();
		$subsets = 'latin,latin-ext';

		/* translators: If there are characters in your language that are not supported by Roboto, translate this to 'off'. Do not translate into your own language. */
		if ('off' !== _x('on', 'Roboto font: on or off', 'iflow')) {
			$fonts[] = 'Roboto';
		}

		/* translators: If there are characters in your language that are not supported by Open Sans, translate this to 'off'. Do not translate into your own language. */
		if ('off' !== _x('on', 'Open Sans font: on or off', 'iflow')) {
			$fonts[] = 'Open+Sans:700';
		}

		if ($fonts) {
			$fonts_url = add_query_arg(array('family' => urlencode(implode('|', $fonts)), 'subset' => urlencode($subsets), ), 'https://fonts.googleapis.com/css');
		}

		return $fonts_url;
	}

endif;

/**
 * Enqueues scripts and styles.
 *
 * @since iFlow 1.0
 */
function iflow_scripts() {
	// Bootstrap framework
	wp_enqueue_style('bootstrap-main', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6');

	// Normalize framework
	wp_enqueue_style('normalize-css', get_template_directory_uri() . '/css/normalize.css', array(), '4.1.1');

	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style('iflow-fonts', iflow_fonts_url(), array(), null);

	// Add Fontawesome, used in the main stylesheet.
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.4.0');

	// Add Hover CSS, used in the main stylesheet.
	wp_enqueue_style('hover-css', get_template_directory_uri() . '/css/hover.min.css', array(), '2.0.1');

	// Theme stylesheet.
	wp_enqueue_style('iflow-style', get_stylesheet_uri());

	// Load jQuery library
	wp_enqueue_script('iflow-jquery-main', get_template_directory_uri() . '/js/jquery-2.2.4.min.js', array(), '2.2.4', true);

	// Load bootstrap js
	wp_enqueue_script('iflow-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.6', true);

	// Load jQuery easing
	wp_enqueue_script('iflow-jquery-easing', get_template_directory_uri() . '/js/jquery.easing.1.3.js', array(), '1.3', true);

	// Load the html5 shiv.
	wp_enqueue_script('iflow-html5', get_template_directory_uri() . '/js/html5shiv.min.js', array(), '3.7.2');
	wp_script_add_data('iflow-html5', 'conditional', 'lt IE 9');

	// Load the respond.js.
	wp_enqueue_script('iflow-respond', get_template_directory_uri() . '/js/respond.min.js', array(), '1.4.2');
	wp_script_add_data('iflow-respond', 'conditional', 'lt IE 9');

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	// Load All Programs script
	if ( is_page('all-programs') ) {
		wp_enqueue_script('iflow-jquery-all-programs', get_template_directory_uri() . '/js/programs.js', array(), '1.0', true);
		wp_localize_script( 'iflow-jquery-all-programs', 'programs', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		));
	}

	// Load jQuery customs script
	wp_enqueue_script('iflow-jquery-custom', get_template_directory_uri() . '/js/custom.js', array(), '1.0', true);
}

add_action('wp_enqueue_scripts', 'iflow_scripts');



?>