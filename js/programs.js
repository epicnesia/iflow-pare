$(document).ready(function() {

	//============================= All Programs Script ==================================
	$(".cat-programs").change(function() {
		$("#program-posts").slideUp("slow", function() {
			
			var cats = [];
			$('input.cat-programs:checked').each(function(i){
				cats[i] = $(this).val();
	        });
			
			$.ajax({
				url : programs.ajax_url,
				type : 'post',
				data : {
					action : 'get_cat_posts',
					cat_id : cats
				},
			}).done(function(r) {
				$("#program-posts").html(r);
				$("#program-posts").slideDown("slow");
			})
		});
	});

});
