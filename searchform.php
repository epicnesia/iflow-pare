<form class="search-form" action="<?php echo esc_url(home_url('/')); ?>" method="get">
	<input type="text" name="s" placeholder="To search type here and hit enter.." />
	<input type="submit" value="Search" />
</form>