<?php get_header(); ?>

<section id="content">
	<div class="container">
		<div class="row">
			<center>
				<h1>Error</h1>
				<img class="img-responsive" src="<?php echo get_template_directory_uri().'/img/404.png'; ?>" alt="404 page not found" width="" height="" />
				&nbsp;
				<h4>Page not found, back to <a href="<?php echo esc_url( home_url() ); ?>">Homepage</a></h4>
			</center>
		</div>
	</div>
</section>

<?php get_footer(); ?>