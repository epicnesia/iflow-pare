<footer class="container-fluid">
	<div class="row">
		<div class="container">
			<div class="row">
				<?php
					if ( is_active_sidebar( 'footer-1' ) ) :
				?>
					<div id="footer-widget" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php dynamic_sidebar('footer-1'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="copyright">
			<a class="copyright-img" href="<?php echo esc_url(home_url()); ?>"> <img class="image-responsive" src="<?php echo get_template_directory_uri() . '/img/logo-mini.png'; ?>" alt="<?php echo get_bloginfo('name'); ?>" width="" height="" /> </a>
			<p class="copyright-text"><a href="<?php echo esc_url(home_url()); ?>">Copyright &copy; <?php echo date('Y'); ?> iFlow Pare. All rights reserved.</a></p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>