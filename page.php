<?php get_header(); ?>

<section id="content">
	
	<?php 
		
		if (have_posts()) : while (have_posts()) : the_post(); 
						
			$the_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
				
			if(isset($the_url[0])) :
					

	?>
	
	<div class="container-fluid">
		<div class="row">

			<img src="<?php echo $the_url[0]; ?>" class="single-post-featured-image" width="" height="" alt="Featured image of <?php the_title(); ?>" />
			
			<?php
			else:
			?>
			
			<img src="<?php echo get_template_directory_uri() . '/img/placeholder-img-large.jpg'; ?>" class="single-post-featured-image" width="" height="" alt="Featured image of <?php the_title(); ?>" />

			<?php
			endif;
			?>
			
		</div>
	</div>
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 single-post-content-wrapper">
				<div class="col-lg-4 col-md-4 col-sm-4 hidden-xs single-post-author">
					<div class="single-post-author-avatar">
						<?php echo get_avatar($post->post_author); ?>
					</div>
					<div class="single-post-author-description">
						<?php echo the_author_meta('description', $post->post_author); ?>
					</div>
					<div class="share-button">
						<h4>Share</h4>
						<div class="share-link-list">
							<a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>&title=<?php echo get_bloginfo( 'name' ); ?> <?php wp_title(); ?>" onclick="window.open(this.href, 'mywin', 'width=600,height=600,toolbar=1,resizable=0'); return false;" ><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href="http://twitter.com/intent/tweet?text=<?php echo get_bloginfo( 'name' ); ?> <?php wp_title(); ?>&url=<?php echo get_permalink(); ?>&via=iflowpare&hashtags=kampungInggris,iflowPare" onclick="window.open(this.href, 'mywin', 'width=600,height=600,toolbar=1,resizable=0'); return false;" ><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<a href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>" onclick="window.open(this.href, 'mywin', 'width=600,height=600,toolbar=1,resizable=0'); return false;" ><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						</div>
					</div>
					<?php get_sidebar(); ?>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 single-post-content">
					<h1 class="single-post-title"><?php the_title(); ?></h1>
					<div class="single-post-title-divider"></div>
					<?php the_content(); ?>
				</div>
			</div>

			<?php
			endwhile;
			endif;
			?>
				
			<?php 
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif; 
			?>
			
		</div>
	</div>
</section>

<?php get_footer(); ?>