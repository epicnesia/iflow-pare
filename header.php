<!DOCTYPE html>
<html <?php language_attributes() ?>>
	<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title><?php echo get_bloginfo( 'name' ); ?> <?php wp_title(); ?></title>
  	
  	<link rel="profile" href="http://gmpg.org/xfn/11">
  	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() . '/img/logo-mini.png'; ?>">

	<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
	?>
	</head>
	
	<body>
		
		<div class="container-fluid search-form-wrapper" style="display:none;">
			<div class="row">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<?php get_search_form(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<nav class="navbar navbar-custom container">
			<div class="row">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>"> <img class="image-responsive" src="<?php echo get_template_directory_uri().'/img/logo-mini.png'; ?>" alt="<?php echo get_bloginfo('name'); ?>" width="" height="" /> </a>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="fa fa-lg fa-bars"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="navbar-collapse-1">
					<div class="navbar-phone">
						<i class="fa fa-phone" aria-hidden="true"></i> 0857 0403 8090
					</div>
					<?php 
						 
						//* Primary navigation */
						wp_nav_menu(array('menu' => 'top_menu', 'depth' => 2, 'container' => false, 'menu_class' => 'nav navbar-nav',
						//Process nav menu using our custom nav walker
						'walker' => new wp_bootstrap_navwalker()));
						
					?>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		
		
	<div class="container-fluid breadcrumb-wrapper">
		<div class="row">
			<div class="container">
				<?php my_breadcrumbs(); ?>
			</div>
		</div>
	</div>