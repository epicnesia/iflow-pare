<?php 
/**
 * Template Name: All Programs Page
 *
 * Description: A custom page template for displaying all programs.
 *
 * @package iFlow
 * @since 1.0
 */ 
?>

<?php get_header(); ?>

<section id="content">
	
	<?php 
		
		if (have_posts()) : while (have_posts()) : the_post(); 
					

	?>
	<div class="container">
		<div class="row">
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-12">
					<div class="col-lg-12">
						<div class="row">
							<h4>Programs</h4>
							<ul id="program-posts" class="list-unstyled list-programs">
								<?php
								
									// The Query
									$the_query = new WP_Query( array( 'posts_per_page' => '-1', 'category_name' => 'programs', 'orderby' => 'name', 'order' => 'ASC' ) );
									
									// The Loop
									if ( $the_query->have_posts() ) {
										
										while ( $the_query->have_posts() ) {
											$the_query->the_post();
											echo '<li><a href="'.get_permalink().'"><i class="fa fa-check" aria-hidden="true"></i> ' . get_the_title() . '</a></li>';
										}
										
										wp_reset_postdata();
									}
								
								?>
							</ul>
						</div>
					</div>
					<div id="program-cats" class="col-lg-12">
						<div class="row">
							<h4>Filter / Category</h4>
							<ul class="list-unstyled list-programs">
								<?php
								
									$cats = get_categories(array('parent'=>'2'));
									
									foreach ($cats as $cat) {
								?>
								<li>
									<div class="checkbox">
										  <label>
										    <input class="cat-programs" type="checkbox" name="catPrograms[]" value="<?php echo $cat->term_id; ?>">
										    <?php
													echo $cat->name;
											?>
										  </label>
									</div>
								</li>
								<?php
									}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<?php
			endwhile;
			endif;
			?>
			
		</div>
	</div>
</section>

<?php get_footer(); ?>